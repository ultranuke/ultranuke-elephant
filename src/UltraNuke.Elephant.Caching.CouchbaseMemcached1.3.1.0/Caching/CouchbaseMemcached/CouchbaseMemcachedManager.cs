﻿using Couchbase;
using Enyim.Caching.Memcached;

namespace UltraNuke.Caching.CouchbaseMemcached
{
    public class CouchbaseMemcachedManager : ICache
    {
        #region Private Fields
        private ICouchbaseClient cache;
        #endregion

        #region Constructors
        public CouchbaseMemcachedManager()
        {
            cache = new CouchbaseClient();
        }
        #endregion

        #region Public Members
        public bool Contains(string key)
        {
            return cache.KeyExists(key);
        }

        public void Set(string key, object value)
        {
            cache.Store(StoreMode.Set, key, value);
        }

        public void Set<T>(string key, T value)
        {
            cache.Store(StoreMode.Set, key, value);
        }

        public object Get(string key)
        {
            return cache.Get(key);
        }

        public T Get<T>(string key)
        {
            return cache.Get<T>(key);
        }

        public void Remove(string key)
        {
            cache.Remove(key);
        }
        #endregion
    }
}
