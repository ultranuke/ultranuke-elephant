﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using NHibernateCfg = NHibernate.Cfg;
using System.Linq.Expressions;
using System.Reflection;
using System;

namespace UltraNuke.Repositories.NHibernate
{
    public class NHibernateContext : DisposableObject, INHibernateContext
    {
        #region Private Fields
        private readonly NHibernateSessionFactory sessionFactory;
        private readonly ISession session = null;
        private ITransaction transaction;
        #endregion

        #region Constructors
        public NHibernateContext()
            : this(new NHibernateCfg.Configuration().Configure())
        {
        }
        public NHibernateContext(NHibernateCfg.Configuration nhibernateConfig)
        {
            sessionFactory = new NHibernateSessionFactory(nhibernateConfig);
            session = sessionFactory.Session;
        }
        #endregion

        #region Private Methods
        private void SetupTransaction()
        {
            if (transaction != null)
                transaction.Dispose();
            transaction = session.BeginTransaction();
        }

        private void Flush()
        {
            this.session.Flush();
        }
        #endregion

        #region Protected Methods
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                    transaction = null;
                }
                ISession dbSession = session;
                if (dbSession != null)
                {
                    dbSession.Dispose();
                    dbSession = null;
                }
            }
        }
        #endregion

        #region INHibernateContext Members
        public T Get<T>(object id)
        {
            var result = this.session.Get<T>(id);
            return result;
        }

        public IEnumerable<T> Query<T>()
        {
            List<T> result = this.session.Query<T>().ToList();
            return result;
        }

        public IEnumerable<T> Query<T>(Expression<System.Func<T, bool>> where)
        {
            List<T> result = this.session.Query<T>().Where(where).ToList();
            return result;
        }
        #endregion

        #region IRepositoryContext Members
        public void RegisterSave(object obj)
        {
            Type modelType = obj.GetType();
            PropertyInfo pi = modelType.GetProperty("AggregateState", BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (pi != null)
            {
                object val = pi.GetValue(obj, null);
                if (val != null && (AggregateState)val == AggregateState.Deleted)
                {
                    session.Delete(obj);
                }
                else
                {
                    session.SaveOrUpdate(obj);
                }
            }
            else
            {
                session.SaveOrUpdate(obj);
            }
            if (transaction == null)
            {
                Flush();
            }
        }

        public void RegisterNew(object obj)
        {
            session.Save(obj);
            if (transaction == null)
            {
                Flush();
            }
        }

        public void RegisterDeleted(object obj)
        {
            session.Delete(obj);
            if (transaction == null)
            {
                Flush();
            }
        }

        public void RegisterModified(object obj)
        {
            session.Update(obj);
            if (transaction == null)
            {
                Flush();
            }
        }
        #endregion

        #region IUnitOfWork Members
        public bool DTCompatible
        {
            get { return false; }
        }

        public bool Committed
        {
            get
            {
                return transaction != null &&
                    transaction.WasCommitted;
            }
            protected set { }
        }

        public void BeginTransaction()
        {
            SetupTransaction();
        }

        public void Commit()
        {
            if (transaction != null && !transaction.WasCommitted)
            {
                transaction.Commit();
                transaction.Dispose();
                transaction = null;
            }
        }

        public void Rollback()
        {
            if (transaction != null && !transaction.WasRolledBack)
            {
                transaction.Rollback();
                transaction.Dispose();
                transaction = null;
            }
        }
        #endregion
    }
}
