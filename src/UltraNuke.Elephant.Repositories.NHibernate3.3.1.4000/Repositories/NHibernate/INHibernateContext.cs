﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace UltraNuke.Repositories.NHibernate
{
    public interface INHibernateContext : IRepositoryContext
    {
        #region Methods
        T Get<T>(object id);

        IEnumerable<T> Query<T>();

        IEnumerable<T> Query<T>(Expression<System.Func<T, bool>> where);
        #endregion
    }
}
