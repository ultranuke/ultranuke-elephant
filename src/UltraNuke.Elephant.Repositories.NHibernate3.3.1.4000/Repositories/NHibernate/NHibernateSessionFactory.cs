﻿using NHibernate;
using UltraNuke.Caching;
using UltraNuke.Caching.DefaultMemoryCache;
using NHibernateCfg = NHibernate.Cfg;

namespace UltraNuke.Repositories.NHibernate
{
    internal sealed class NHibernateSessionFactory
    {
        #region Private Fields
        private readonly ISessionFactory sessionFactory = null;
        private ISession session = null;
        #endregion

        #region Constructors
        internal NHibernateSessionFactory(NHibernateCfg.Configuration nhibernateConfig)
        {
            sessionFactory = nhibernateConfig.BuildSessionFactory();
        }
        #endregion
        
        #region Private Properties
        private ICache Cache
        {
            get
            {
                return new DefaultMemoryCacheManager();
            }
        }
        #endregion

        #region Public Properties
        public ISession Session
        {
            get
            {
                ISession result = session;
                if (result != null && result.IsOpen)
                    return result;
                return OpenSession();
            }
        }
        #endregion

        #region Public Methods
        public ISession OpenSession()
        {
            this.session = sessionFactory.OpenSession();
            return this.session;
        }
        #endregion

    }
}
