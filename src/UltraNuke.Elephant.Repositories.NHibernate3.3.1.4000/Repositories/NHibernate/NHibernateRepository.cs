﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace UltraNuke.Repositories.NHibernate
{
    public class NHibernateRepository : Repository
    {
        #region Private Fields
        private readonly INHibernateContext nhContext = null;
        #endregion

        #region Constructors
        public NHibernateRepository(IRepositoryContext context)
            : base(context)
        {
            nhContext = context as INHibernateContext;
        }
        #endregion

        #region Protected Methods
        protected override void DoSave<T>(T t)
        {
            this.Context.RegisterSave(t);
        }

        protected override void DoAdd<T>(T t)
        {
            this.Context.RegisterNew(t);
        }

        protected override void DoRemove<T>(T t)
        {
            this.Context.RegisterDeleted(t);
        }

        protected override void DoUpdate<T>(T t)
        {
            this.Context.RegisterModified(t);
        }

        protected override T DoGet<T>(object id)
        {
            return nhContext.Get<T>(id);
        }

        protected override IEnumerable<T> DoQuery<T>()
        {
            return nhContext.Query<T>();
        }

        protected override IEnumerable<T> DoQuery<T>(Expression<System.Func<T, bool>> where)
        {
            return nhContext.Query<T>(where);
        }
        #endregion
    }
}
