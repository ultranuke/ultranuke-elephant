﻿using System;

namespace UltraNuke.Delphinidae
{
    public interface IEntity
    {
        #region Properties
        Guid Id { get; set; }
        #endregion
    }
}
