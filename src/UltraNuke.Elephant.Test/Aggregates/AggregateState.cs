﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UltraNuke.Delphinidae
{
    public enum AggregateState
    {
        Added = 1,
        Modified = 2,
        Deleted = 4
    }
}
