﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using UltraNuke.Delphinidae.Test.Aggregates;

namespace UltraNuke.Delphinidae.Test.Repositories.EntityFramework
{
    public class BaseTestDomainMap : EntityTypeConfiguration<BaseTestDomain>
    {
        public BaseTestDomainMap()
        {
            this.ToTable("ET_BaseTestDomain");

            this.Ignore(t => t.AggregateState);
        }
    }
}
