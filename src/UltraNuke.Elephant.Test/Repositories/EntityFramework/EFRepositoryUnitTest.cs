﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UltraNuke.Delphinidae.Repositories;
using UltraNuke.Delphinidae.Repositories.EntityFramework;
using System.Linq;
using UltraNuke.Delphinidae.Test.Aggregates;

namespace UltraNuke.Delphinidae.Test.Repositories.EntityFramework
{
    [TestClass]
    public class EFRepositoryUnitTest:UnitTestBase
    {

        private static IRepository<BaseTestDomain> GetRepository()
        {
            EFTestContext efc = new EFTestContext();
            IRepositoryContext rct = new EntityFrameworkContext(efc);
            IRepository<BaseTestDomain> repository = new EntityFrameworkRepository<BaseTestDomain>(rct);
            return repository;
        }

        [TestMethod]
        public void ExecuteCRUD()
        {
            Guid id;
            IRepository<BaseTestDomain> repository = GetRepository();
            BaseTestDomain createDomain = BaseTestDomain.Create("T0001", "测试项目001");
            repository.Save(createDomain);
            id = createDomain.Id;
            repository = GetRepository();
            BaseTestDomain expectedDomain = repository.Get(id);
            Assert.AreEqual("测试项目001", expectedDomain.Domain);

            repository = GetRepository();
            BaseTestDomain updateDomain = repository.Get(id);
            updateDomain.UpdateInformation("T0001", "测试项目002");
            repository.Save(updateDomain);
            repository = GetRepository();
            expectedDomain = repository.Get(id);
            Assert.AreEqual("测试项目002", expectedDomain.Domain);

            repository = GetRepository();
            expectedDomain = repository.Query().Where(w => w.Code.Equals("T0001")).FirstOrDefault();
            Assert.AreEqual("测试项目002", expectedDomain.Domain);

            repository = GetRepository();
            BaseTestDomain deleteDomain = repository.Get(id);
            deleteDomain.Delete();
            repository.Save(deleteDomain);
            repository = GetRepository();
            expectedDomain = repository.Get(id);
            Assert.IsNull(expectedDomain);
        }
    }
}
