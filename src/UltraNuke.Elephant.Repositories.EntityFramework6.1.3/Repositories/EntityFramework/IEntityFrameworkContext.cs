﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Expressions;

namespace UltraNuke.Repositories.EntityFramework
{
    public interface IEntityFrameworkContext : IRepositoryContext
    {
        #region Methods
        T Get<T>(object id) where T : class;

        IEnumerable<T> Query<T>() where T : class;

        IEnumerable<T> Query<T>(Expression<System.Func<T, bool>> where) where T : class;
        #endregion
    }
}
