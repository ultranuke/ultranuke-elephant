﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System;
using System.Data.Entity;

namespace UltraNuke.Repositories.EntityFramework
{
    public class EntityFrameworkContext : DisposableObject, IEntityFrameworkContext
    {
        #region Private Fields
        private readonly DbContext efContext;
        private readonly object sync = new object();
        private bool isCommit = false;
        #endregion

        #region Constructors
        public EntityFrameworkContext(DbContext context)
        {
            this.efContext = context;
        }
        #endregion

        #region Private Methods
        private void SetupTransaction()
        {
            isCommit = true;
        }

        private void SaveOrUpdate(object obj)
        { 
            Type modelType = obj.GetType();
            PropertyInfo pi = modelType.GetProperty("AggregateState", BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (pi != null)
            {
                object val = pi.GetValue(obj, null);
                if (val != null && (AggregateState)val == AggregateState.Added)
                {
                    efContext.Entry(obj).State = EntityState.Added;
                }
                else
                {
                    efContext.Entry(obj).State = EntityState.Modified;
                }
            }
        }
        #endregion

        #region Protected Methods
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                efContext.Dispose();
            }
        }
        #endregion

        #region IEntityFrameworkContext Members
        public T Get<T>(object id) where T : class
        {
            var result = efContext.Set<T>().Find(id);
            return result;
        }

        public IEnumerable<T> Query<T>() where T : class
        {
            List<T> result = efContext.Set<T>().ToList();
            return result;
        }

        public IEnumerable<T> Query<T>(Expression<System.Func<T, bool>> where) where T : class
        {
            List<T> result = efContext.Set<T>().Where(where).ToList();
            return result;
        }
        #endregion

        #region IRepositoryContext Members
        public void RegisterSave(object obj)
        {
            Type modelType = obj.GetType();
            PropertyInfo pi = modelType.GetProperty("AggregateState", BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (pi != null)
            {
                object val = pi.GetValue(obj, null);
                if (val != null && (AggregateState)val == AggregateState.Deleted)
                {
                    efContext.Entry(obj).State = EntityState.Deleted;
                }
                else
                {
                    SaveOrUpdate(obj);
                }
            }
            else
            {
                SaveOrUpdate(obj);
            }
            if (isCommit)
            {
                Committed = false;
            }
            else
            {
                efContext.SaveChanges();
            }
        }

        public void RegisterNew(object obj)
        {
            efContext.Entry(obj).State = EntityState.Added;
            if (isCommit)
            {
                Committed = false;
            }
            else
            {
                efContext.SaveChanges();
            }
        }

        public void RegisterDeleted(object obj)
        {
            efContext.Entry(obj).State = EntityState.Deleted;
            if (isCommit)
            {
                Committed = false;
            }
            else
            {
                efContext.SaveChanges();
            }
        }

        public void RegisterModified(object obj)
        {
            efContext.Entry(obj).State = EntityState.Deleted;
            if (isCommit)
            {
                Committed = false;
            }
            else
            {
                efContext.SaveChanges();
            }
        }
        #endregion

        #region IUnitOfWork Members
        public bool DTCompatible
        {
            get { return false; }
        }

        private bool committed;
        public bool Committed
        {
            get
            {
                return committed;
            }
            protected set { committed = value; }
        }

        public void BeginTransaction()
        {
            SetupTransaction();
        }

        public void Commit()
        {
            if (isCommit && !committed)
            {
                lock (sync)
                {
                    efContext.SaveChanges();
                }
                Committed = true;
            }
        }

        public void Rollback()
        {
            Committed = false;
        }
        #endregion
    }
}
