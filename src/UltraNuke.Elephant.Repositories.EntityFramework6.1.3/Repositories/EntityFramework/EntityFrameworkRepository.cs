﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace UltraNuke.Repositories.EntityFramework
{
    public class EntityFrameworkRepository : Repository
    {
        #region Private Fields
        private readonly IEntityFrameworkContext efContext = null;
        #endregion

        #region Constructors
        public EntityFrameworkRepository(IRepositoryContext context)
            : base(context)
        {
            efContext = context as IEntityFrameworkContext;
        }
        #endregion

        #region Protected Methods
        protected override void DoSave<T>(T t)
        {
            this.Context.RegisterSave(t);
        }

        protected override void DoAdd<T>(T t)
        {
            this.Context.RegisterNew(t);
        }

        protected override void DoRemove<T>(T t)
        {
            this.Context.RegisterDeleted(t);
        }

        protected override void DoUpdate<T>(T t)
        {
            this.Context.RegisterModified(t);
        }

        protected override T DoGet<T>(object id)
        {
            return efContext.Get<T>(id);
        }

        protected override IEnumerable<T> DoQuery<T>()
        {
            return efContext.Query<T>();
        }

        protected override IEnumerable<T> DoQuery<T>(Expression<System.Func<T, bool>> where)
        {
            return efContext.Query<T>(where);
        }
        #endregion
    }
}
