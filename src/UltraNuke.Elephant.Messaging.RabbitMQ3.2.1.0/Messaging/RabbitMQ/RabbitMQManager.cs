﻿using System;
using UltraNuke.Messaging;

namespace UltraNuke.Messaging.RabbitMQ
{
    public class RabbitMQManager : DisposableObject, IMessageQueue
    {
        #region Protected Methods
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
        #endregion

        public void Send<T>(string queue, T message) where T : class
        {
            throw new NotImplementedException();
        }

        public T Receive<T>(string queue) where T : class
        {
            throw new NotImplementedException();
        }
    }
}
