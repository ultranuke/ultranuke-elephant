﻿using System.Runtime.Caching;

namespace UltraNuke.Caching.DefaultMemoryCache
{
    public class DefaultMemoryCacheManager : ICache
    {
        #region Private Fields
        private ObjectCache cache = MemoryCache.Default;
        #endregion

        #region Public Members
        public bool Contains(string key)
        {
            return cache.Contains(key);
        }

        public void Set(string key, object value)
        {            
            CacheItemPolicy policy = new CacheItemPolicy();
            cache.Set(key, value, policy);
        }

        public void Set<T>(string key, T value)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            cache.Set(key, value, policy);
        }

        public object Get(string key)
        {
            return cache.Get(key);
        }

        public T Get<T>(string key)
        {
            return (T)cache.Get(key);
        }

        public void Remove(string key)
        {
            cache.Remove(key);
        }
        #endregion
    }
}
