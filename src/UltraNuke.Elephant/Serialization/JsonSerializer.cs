﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace UltraNuke.Serialization
{
    public class JsonSerializer
    {
        #region ISerializer Members
        public byte[] Serialize<T>(T t)
        {
            Type graphType = t.GetType();
            DataContractJsonSerializer js = new DataContractJsonSerializer(graphType);
            byte[] ret = null;
            using (MemoryStream ms = new MemoryStream())
            {
                js.WriteObject(ms, t);
                ret = ms.ToArray();
                ms.Close();
            }
            return ret;
        }

        public T Deserialize<T>(byte[] stream)
        {
            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream(stream))
            {
                T ret = (T)js.ReadObject(ms);
                ms.Close();
                return ret;
            }
        }
        #endregion
    }
}
