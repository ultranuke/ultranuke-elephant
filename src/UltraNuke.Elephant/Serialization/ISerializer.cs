﻿
namespace UltraNuke.Serialization
{
    /// <summary>
    /// ISerializer提供应用程序序列化和反序列化操作的接口
    /// </summary>
    public interface ISerializer
    {
        #region Methods
        byte[] Serialize<T>(T t);

        T Deserialize<T>(byte[] stream);
        #endregion
    }
}
