﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace UltraNuke.Serialization
{
    public class BinarySerializer : ISerializer
    {
        #region Private Fields
        private readonly BinaryFormatter binaryFormatter = new BinaryFormatter();
        #endregion

        #region ISerializer Members
        public byte[] Serialize<T>(T t)
        {
            byte[] ret = null;
            using (MemoryStream ms = new MemoryStream())
            {
                binaryFormatter.Serialize(ms, t);
                ret = ms.ToArray();
                ms.Close();
            }
            return ret;
        }

        public T Deserialize<T>(byte[] stream)
        {
            using (MemoryStream ms = new MemoryStream(stream))
            {
                T ret = (T)binaryFormatter.Deserialize(ms);
                ms.Close();
                return ret;
            }
        }
        #endregion
    }
}
