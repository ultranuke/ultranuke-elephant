﻿using System;
using System.IO;
using NSXmlSerialization = System.Xml.Serialization;

namespace UltraNuke.Serialization
{
    public class XmlSerializer
    {
        #region ISerializer Members
        public byte[] Serialize<T>(T t)
        {
            Type graphType = t.GetType();
            NSXmlSerialization.XmlSerializer xmlSerializer = new NSXmlSerialization.XmlSerializer(graphType);
            byte[] ret = null;
            using (MemoryStream ms = new MemoryStream())
            {
                xmlSerializer.Serialize(ms, t);
                ret = ms.ToArray();
                ms.Close();
            }
            return ret;
        }

        public T Deserialize<T>(byte[] stream)
        {
            NSXmlSerialization.XmlSerializer xmlSerializer = new NSXmlSerialization.XmlSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream(stream))
            {
                T ret = (T)xmlSerializer.Deserialize(ms);
                ms.Close();
                return ret;
            }
        }
        #endregion
    }
}
