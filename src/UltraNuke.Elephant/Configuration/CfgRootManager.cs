﻿using System;
using System.Xml;
using System.Xml.XPath;
using Common.Logging;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 提供应用程序配置操作
    /// </summary>
    public class CfgRootManager
    {
        #region Private Fields
        private ILog log = LogManager.GetLogger(typeof(CfgRootManager));
        #endregion

        #region Constructors
        private CfgRootManager(XmlReader configurationReader)
        {
            XPathNavigator nav;
            try
            {
                nav = new XPathDocument(XmlReader.Create(configurationReader, null)).CreateNavigator();
            }
            catch (CfgException e)
            {
                if (log.IsErrorEnabled)
                    log.Error(e.Message, e);

                throw e;
            }
            catch (Exception ex)
            {
                CfgException e = new CfgException("配置发生错误。", ex);

                if (log.IsErrorEnabled)
                    log.Error(ex.Message, ex);

                throw e;
            }

            Parse(nav);
        }
        #endregion

        #region Public Properties
        private CfgPropertiesManager propertiesManager;
        /// <summary>
        /// 属性集合
        /// </summary>
        public CfgPropertiesManager PropertiesManager
        {
            set { propertiesManager = value; }
            get { return propertiesManager; }
        }
        #endregion

        #region Private Methods
        private void Parse(XPathNavigator navigator)
        {
            XPathNavigator xpn = navigator.SelectSingleNode(XmlHelper.PropertiesExpression);
            if (xpn != null)
            {
                propertiesManager = new CfgPropertiesManager(navigator);
            }
            else
            {
                CfgException e = new CfgException("配置文件中没有发现标签<properties xmlns='" + XmlHelper.SchemaXmlns + "'>。");
                LogAndThrow(e);
            }
        }

        private void LogAndThrow(Exception exception)
        {
            if (log.IsErrorEnabled)
                log.Error(exception.Message, exception);

            throw exception;
        }
        #endregion

        #region Internal Methods
        internal static CfgRootManager FromAppConfig(XmlNode node)
        {
            XmlTextReader reader = new XmlTextReader(node.OuterXml, XmlNodeType.Document, null);
            return new CfgRootManager(reader);
        }
        #endregion
    }
}
