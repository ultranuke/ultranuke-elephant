﻿using System.Collections.Generic;
using System.Configuration;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 提供应用程序配置操作
    /// </summary>
    public class CfgManager : ICfg
    {
        #region Private Fields
        private static CfgManager instance;
        private static object lockObject = new object();
        #endregion

        #region Constructors
        private CfgManager()
        {
            properties = new Dictionary<string, string>();
            LoadGlobalPropertiesFromAppConfig();
        }
        #endregion

        #region Public Properties
        private IDictionary<string, string> properties;
        /// <summary>
        /// 属性集合
        /// </summary>
        public IDictionary<string, string> Properties
        {
            get { return properties; }
        }
        #endregion

        #region Private Methods
        private void LoadGlobalPropertiesFromAppConfig()
        {
            CfgRootManager configurationManager = ConfigurationManager.GetSection(XmlHelper.SectionName) as CfgRootManager;

            foreach (KeyValuePair<string, string> kvp in configurationManager.PropertiesManager.Properties)
            {
                properties[kvp.Key] = kvp.Value;
            }
        }
        #endregion

        #region Public Methods
        public static CfgManager GetCfgger()
        {
            if (instance == null)
            {
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        instance = new CfgManager();
                    }
                }
            }
            return instance;
        }
        #endregion
    }
}
