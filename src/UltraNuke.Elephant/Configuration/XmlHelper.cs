﻿using System.Xml;
using System.Xml.XPath;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 配置相关的XML帮助器
    /// </summary>
    public static class XmlHelper
    {
        #region Private Fields
        private const string RootPrefixPath = "//" + NamespacePrefix + ":";
        private const string ChildPrefixPath = NamespacePrefix + ":";
        #endregion

        #region Public Fields
        public const string SectionName = "ultranuke";
        public const string SchemaXmlns = "urn:ultranuke";
        public const string NamespacePrefix = "cfg";

        public static readonly XPathExpression PropertiesPropertyExpression;
        public static readonly XPathExpression PropertiesExpression;
        #endregion

        #region Constructors
        static XmlHelper()
        {
            NameTable nt = new NameTable();
            XmlNamespaceManager nsMgr = new XmlNamespaceManager(nt);
            nsMgr.AddNamespace(NamespacePrefix, SchemaXmlns);

            PropertiesExpression = XPathExpression.Compile(RootPrefixPath + "properties", nsMgr);
            PropertiesPropertyExpression = XPathExpression.Compile(RootPrefixPath + "properties/" + ChildPrefixPath + "property", nsMgr);
        }
        #endregion
    }
}
