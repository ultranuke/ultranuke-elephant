﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// 提供应用程序配置属性相关操作
    /// </summary>
    public class CfgPropertiesManager
    {

        #region Constructors
        internal CfgPropertiesManager(XPathNavigator configurationSection)
        {
            if (configurationSection == null)
                throw new ArgumentNullException("configurationSection");

            Parse(configurationSection);
        }
        #endregion

        #region Public Properties
        private IDictionary<string, string> properties = new Dictionary<string, string>();
        /// <summary>
        /// 属性集合
        /// </summary>
        public IDictionary<string, string> Properties
        {
            get { return properties; }
        }
        #endregion

        #region Private Methods
        private void Parse(XPathNavigator navigator)
        {
            ParseProperties(navigator);
        }

        private void ParseProperties(XPathNavigator navigator)
        {
            XPathNodeIterator xpni = navigator.Select(XmlHelper.PropertiesPropertyExpression);
            while (xpni.MoveNext())
            {
                string propName;
                string propValue;
                if (xpni.Current.Value != null)
                {
                    propValue = xpni.Current.Value.Trim();
                }
                else
                {
                    propValue = string.Empty;
                }
                XPathNavigator pNav = xpni.Current.Clone();
                pNav.MoveToFirstAttribute();
                propName = pNav.Value;
                if (!string.IsNullOrEmpty(propName))
                    properties[propName] = propValue;
            }
        }
        #endregion
    }
}
