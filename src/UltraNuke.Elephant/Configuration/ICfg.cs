﻿using System.Collections.Generic;

namespace UltraNuke.Configuration
{
    /// <summary>
    /// ICfg提供应用程序配置操作的接口
    /// </summary>
    public interface ICfg
    {
        #region Properties
        /// <summary>
        /// 属性集合
        /// </summary>
        IDictionary<string, string> Properties { get; }
        #endregion
    }
}
