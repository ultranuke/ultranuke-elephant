﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace UltraNuke.Repositories
{
    public abstract class Repository : IRepository
    {
        #region Private Fields
        private readonly IRepositoryContext context;
        #endregion

        #region Constructors
        public Repository(IRepositoryContext context)
        {
            this.context = context;
        }
        #endregion

        #region Protected Methods
        protected abstract void DoSave<T>(T t);
        protected abstract void DoAdd<T>(T t);
        protected abstract void DoRemove<T>(T t);
        protected abstract void DoUpdate<T>(T t);
        protected abstract T DoGet<T>(object id) where T : class;
        protected abstract IEnumerable<T> DoQuery<T>() where T : class;
        protected abstract IEnumerable<T> DoQuery<T>(Expression<Func<T, bool>> where) where T : class;
        #endregion

        #region IRepository<T> Members
        public IRepositoryContext Context
        {
            get { return this.context; }
        }

        public void Save<T>(T t) where T : class
        {
            this.DoSave(t);
        }

        public void Add<T>(T t) where T : class
        {
            this.DoAdd(t);
        }

        public void Remove<T>(T t) where T : class
        {
            this.DoRemove(t);
        }

        public void Update<T>(T t) where T : class
        {
            this.DoUpdate(t);
        }

        public T Get<T>(object id) where T : class
        {
            return this.DoGet<T>(id);
        }

        public IEnumerable<T> Query<T>() where T : class
        {
            return this.DoQuery<T>();
        }

        public IEnumerable<T> Query<T>(Expression<Func<T, bool>> where) where T : class
        {
            return this.DoQuery<T>(where);
        }
        #endregion


    }
}
