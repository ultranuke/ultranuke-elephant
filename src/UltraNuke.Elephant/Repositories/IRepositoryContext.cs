﻿using System;
using UltraNuke.UnitOfWork;

namespace UltraNuke.Repositories
{
    public interface IRepositoryContext : IUnitOfWork, IDisposable
    {
        #region Methods
        void RegisterSave(object obj);
        void RegisterNew(object obj);
        void RegisterModified(object obj);
        void RegisterDeleted(object obj);
        #endregion
    }
}
