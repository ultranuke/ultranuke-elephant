﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace UltraNuke.Repositories
{
    /// <summary>
    /// IRepository提供应用程序仓储模式基本操作的接口
    /// </summary>
    public interface IRepository
    {
        #region Properties
        IRepositoryContext Context { get; }
        #endregion

        #region Methods
        void Save<T>(T t) where T : class;

        void Add<T>(T t) where T : class;

        void Remove<T>(T t) where T : class;

        void Update<T>(T t) where T : class;

        T Get<T>(object Id) where T : class;

        IEnumerable<T> Query<T>() where T : class;

        IEnumerable<T> Query<T>(Expression<Func<T, bool>> where) where T : class;
        #endregion
    }
}
