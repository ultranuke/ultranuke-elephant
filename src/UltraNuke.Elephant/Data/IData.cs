﻿using System.Collections.Generic;
using System.Data;

namespace UltraNuke.Data
{
    /// <summary>
    /// IData提供应用程序数据操作的接口
    /// </summary>
    public interface IData
    {
        #region Methods
        int Execute(IDbConnection cnn, string sql, dynamic param = null, IDbTransaction transaction = null, CommandType? commandType = null);

        IEnumerable<T> Query<T>(IDbConnection cnn, string sql, dynamic param = null, IDbTransaction transaction = null, CommandType? commandType = null);
        #endregion
    }
}
