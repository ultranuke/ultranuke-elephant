﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 编号生成器接口。
    /// </summary>
    public interface INumberGenerator
    {
        /// <summary>
        /// 生成编号。
        /// </summary>
        string Generate();

        /// <summary>
        /// 回收编号。
        /// </summary>
        /// <param name="number"></param>
        void Recycle(string number);
    }
}
