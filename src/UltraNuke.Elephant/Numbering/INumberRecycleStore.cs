﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 回收编号仓储接口。
    /// </summary>
    public interface INumberRecycleStore
    {
        /// <summary>
        /// 添加编号到回收站
        /// </summary>
        /// <param name="seedKey"></param>
        /// <param name="number"></param>
        void Add(string seedKey,string number);

        /// <summary>
        /// 从回收站移除编号
        /// </summary>
        /// <param name="seedKey"></param>
        /// <returns>编号</returns>
        string Remove(string seedKey);
    }
}
