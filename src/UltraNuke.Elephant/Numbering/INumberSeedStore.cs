﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 种子仓储接口。
    /// </summary>
    public interface INumberSeedStore
    {
        /// <summary>
        /// 自增，并返回下一个种子值。
        /// </summary>
        /// <param name="seedKey"></param>
        /// <returns></returns>
        int NextSeed(string seedKey);
    }
}
