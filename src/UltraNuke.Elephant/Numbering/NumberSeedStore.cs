﻿using Microsoft.Practices.ServiceLocation;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UltraNuke.Data;

namespace UltraNuke.Numbering
{
    /// <summary>
    /// 种子仓储。
    /// </summary>
    public sealed class NumberSeedStore : INumberSeedStore
    {
        #region Private Fields
        private readonly IDbConnection cnn = null;
        private readonly IData data = null;
        #endregion

        #region Constructors
        public NumberSeedStore(IDbConnection cnn)
        {
            this.cnn = cnn;
            this.data = Data;
        }
        #endregion

        #region Private Properties
        private IData Data
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IData>();
            }
        }
        #endregion

        #region Private Classes
        public class Seed
        {
            public int SeedId { get; set; }

            public string SeedKey { get; set; }

            public int SeedValue { get; set; }
        }
        #endregion

        #region INumberSeedStore Members
        public int NextSeed(string seedKey)
        {
            Seed seed = data.Query<Seed>(
                cnn, 
                @"SELECT * FROM [AF_NumberSeed] WHERE [SeedKey]=@SeedKey", 
                new { SeedKey = seedKey }
                ).SingleOrDefault();

            if (seed == null)
            {
                data.Execute(
                cnn,
                @"INSERT INTO [AF_NumberSeed]([SeedKey],[SeedValue])
                  VALUES (@SeedKey,@SeedValue)",
                new { SeedKey = seedKey, SeedValue = 1 });

                return 1;
            }
            else
            {
                seed.SeedValue++;
                var result = data.Execute(
                cnn,
                @"UPDATE [AF_NumberSeed] SET [SeedValue]=@SeedValue WHERE [SeedId]=@SeedId",
                new { SeedValue = seed.SeedValue, SeedId = seed.SeedId });
                if (result == 0)
                {
                    return this.NextSeed(seedKey);
                }
                return seed.SeedValue;
            }
        }
        #endregion
    }
}
