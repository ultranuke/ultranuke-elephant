﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltraNuke.Numbering
{
    public class NumberGenerator : INumberGenerator
    {
        #region Private Fields
        private INumberRecycleStore recycleStore;
        private INumberSeedStore seedStore;
        private string seedKey;
        private string format;
        private Dictionary<string, string> dictFormatData;
        private DateTime numberDate;
        private int seedWidth;
        #endregion

        #region Constructors
        public NumberGenerator(INumberRecycleStore recycleStore, INumberSeedStore seedStore, DateTime numberDate, string seedKey, string format, Dictionary<string, string> dictFormatData = null, int seedWidth=5)
        {
            seedKey = seedKey.Replace(":yyyy", numberDate.ToString("yyyy"));
            seedKey = seedKey.Replace(":MM", numberDate.ToString("MM"));
            seedKey = seedKey.Replace(":dd", numberDate.ToString("dd"));

            this.recycleStore = recycleStore;
            this.seedStore = seedStore;
            this.numberDate = numberDate;
            this.seedKey = seedKey;
            this.format = format;
            this.dictFormatData = dictFormatData;
            this.seedWidth = seedWidth;
        }
        #endregion

        #region INumberGenerator Members
        public string Generate()
        {
            string number = recycleStore.Remove(seedKey);
            if (string.IsNullOrEmpty(number))
            {
                int seed = seedStore.NextSeed(seedKey);
                string seedNumber = seed.ToString().PadLeft(seedWidth, '0');
                number = format;
                number = number.Replace(":seed", seedNumber);
                number = number.Replace(":yyyy", numberDate.ToString("yyyy"));
                number = number.Replace(":MM", numberDate.ToString("MM"));
                number = number.Replace(":dd", numberDate.ToString("dd"));
                if (dictFormatData != null)
                {
                    foreach (KeyValuePair<string, string> formatData in dictFormatData)
                    {
                        number = number.Replace(formatData.Key, formatData.Value);
                    }
                }
            }
            return number;
        }

        public void Recycle(string number)
        {
            recycleStore.Add(seedKey, number);
        }
        #endregion
    }
}
