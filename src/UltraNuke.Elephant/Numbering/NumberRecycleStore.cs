﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltraNuke.Data;

namespace UltraNuke.Numbering
{
    public class NumberRecycleStore : INumberRecycleStore
    {
        #region Private Fields
        private readonly IDbConnection cnn = null;
        private readonly IData data = null;
        #endregion

        #region Constructors
        public NumberRecycleStore(IDbConnection cnn)
        {
            this.cnn = cnn;
            this.data = Data;
        }
        #endregion

        #region Private Properties
        private IData Data
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IData>();
            }
        }
        #endregion

        #region Private Classes
        public class Recycle
        {
            public int RecycleId { get; set; }

            public string SeedKey { get; set; }

            public string Number { get; set; }
        }
        #endregion

        public void Add(string seedKey, string number)
        {
            data.Execute(
                cnn,
                @"INSERT INTO [AF_NumberRecycle]([SeedKey],[Number])
                  VALUES (@SeedKey,@Number)",
                new { SeedKey = seedKey, Number = number });
        }

        public string Remove(string seedKey)
        {
            var recycle = data.Query<Recycle>(
                   cnn,
                   @"SELECT TOP 1 [RecycleId],[SeedKey],[Number] FROM [AF_NumberRecycle] WHERE [SeedKey]=@SeedKey Order BY [Number]",
                   new { SeedKey = seedKey }
                   ).SingleOrDefault();

            if (recycle == null)
            {
                return string.Empty;
            }
            else
            {
                data.Execute(
                    cnn,
                    @"DELETE FROM [AF_NumberRecycle] WHERE [RecycleId]=@RecycleId",
                new { RecycleId = recycle.RecycleId });
                return recycle.Number;
            }
        }
    }
}
