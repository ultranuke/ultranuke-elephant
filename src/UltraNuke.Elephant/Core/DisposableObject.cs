﻿using System;
using System.Runtime.ConstrainedExecution;

namespace UltraNuke
{
    public abstract class DisposableObject : CriticalFinalizerObject, IDisposable
    {
        #region Finalization Constructs
        ~DisposableObject()
        {
            this.Dispose(false);
        }
        #endregion

        #region Protected Methods
        protected abstract void Dispose(bool disposing);
        protected void ExplicitDispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            this.ExplicitDispose();
        }
        #endregion
    }
}
