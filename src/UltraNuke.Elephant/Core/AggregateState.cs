﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UltraNuke
{
    public enum AggregateState
    {
        Added = 1,
        Deleted = 2
    }
}
