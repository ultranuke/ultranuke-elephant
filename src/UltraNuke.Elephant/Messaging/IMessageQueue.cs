﻿using System;

namespace UltraNuke.Messaging
{
    public interface IMessageQueue : IDisposable
    {
        void Send<T>(string queue, T message) where T : class;

        T Receive<T>(string queue) where T : class;
    }
}
