﻿
namespace UltraNuke.UnitOfWork
{
    public interface IUnitOfWork
    {
        #region Methods
        bool DTCompatible { get; }

        bool Committed { get; }

        void BeginTransaction();

        void Commit();

        void Rollback();
        #endregion
    }
}
