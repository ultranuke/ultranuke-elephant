﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UltraNuke.Utility
{
    public class RoundUtils
    {
        public static decimal Round(decimal d, decimal decimals)
        {
            if (decimals >= 1)
            {
                int l = decimals.ToString().Length;
                d = decimal.Round(d / (decimals * 10), l, MidpointRounding.ToEven) * (decimals * 10);
            }
            else if (decimals == 0)
            {
                d = decimal.Round(d, 0, MidpointRounding.ToEven);
            }
            else
            {
                int l = decimals.ToString().Length - 2;
                d = decimal.Round(d, l, MidpointRounding.ToEven);
            }
            return d;
        }

        public static decimal Round05(decimal d)
        {
            return decimal.Round(d * 2, 0, MidpointRounding.ToEven) / 2;
        }

        public static decimal Round02(decimal d)
        {
            return decimal.Round(d * 5, 0, MidpointRounding.ToEven) / 5;
        }

        public static decimal Round5(decimal d)
        {
            return (d - d % 5) + 5 * decimal.Round((d % 5) / 5, 0, MidpointRounding.ToEven);
        }
    }
}
