﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace UltraNuke.Utility
{
    public sealed class CryptoUtils
    {
        #region Md5
        public static string Md5Encrypt(string plainText)
        {
            byte[] fromBuffer = Encoding.GetEncoding("UTF-8").GetBytes(string.Concat("20150414", plainText, "0948"));
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] toBuffer = md5Hasher.ComputeHash(fromBuffer);
            return Convert.ToBase64String(toBuffer);
        }
        #endregion
    }
}
