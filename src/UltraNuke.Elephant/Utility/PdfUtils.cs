﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace UltraNuke.Utility
{
    public class PdfUtils
    {
        public class ImageParameter
        {
            public string ImageFile;
            public float AbsoluteX;
            public float AbsoluteY;
            
        }

        public static void GeneratePdf(string templateFile, string configFile, string generateFile, Dictionary<string, string> textDatas, IList<ImageParameter> imageDatas)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(configFile);

            PdfReader pdfReader = new PdfReader(templateFile);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(generateFile, FileMode.Create));

            if (textDatas != null)
            {
                AcroFields pdfFormFields = pdfStamper.AcroFields;
                BaseFont baseFT = BaseFont.CreateFont(@"C:/Windows/Fonts/simsun.ttc,1", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                foreach (KeyValuePair<string, string> textData in textDatas)
                {
                    string path = "//mappings/" + textData.Key;
                    if (xDoc.SelectSingleNode(path) != null)
                    {
                        string key = xDoc.SelectSingleNode(path).InnerText;
                        pdfFormFields.SetFieldProperty(key, "textfont", baseFT, null);
                        pdfFormFields.SetField(key, string.IsNullOrEmpty(textData.Value) ? "——" : textData.Value);
                    }
                }
            }

            if (imageDatas != null)
            {
                PdfContentByte under = pdfStamper.GetUnderContent(1);
                foreach (ImageParameter imageData in imageDatas)
                {
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageData.ImageFile);
                    float percentage = 1;
                    float resizedWidht = image.Width;
                    float resizedHeight = image.Height;
                    while (resizedHeight > 25)
                    {
                        percentage = percentage * 0.9f;
                        resizedHeight = image.Height * percentage;
                        resizedWidht = image.Width * percentage;
                    }
                    image.ScalePercent(percentage * 100);
                    image.SetAbsolutePosition(imageData.AbsoluteX, imageData.AbsoluteY);
                    under.AddImage(image);
                }
            }

            pdfStamper.FormFlattening = true;
            pdfStamper.Close();
            pdfReader.Close();
        }

        public static void GeneratePdfByFormFields(string templatePath, string newFilePath, Dictionary<string, string> parameters)
        {
            PdfReader pdfReader = new PdfReader(templatePath);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFilePath, FileMode.Create));
            AcroFields pdfFormFields = pdfStamper.AcroFields;

            BaseFont baseFT = BaseFont.CreateFont(@"C:/Windows/Fonts/simsun.ttc,1", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                pdfFormFields.SetFieldProperty(parameter.Key, "textfont", baseFT, null);
                pdfFormFields.SetField(parameter.Key, string.IsNullOrEmpty(parameter.Value) ? "——" : parameter.Value);
            }

            pdfStamper.Close();
            pdfReader.Close();
        }

        public static void GeneratePdfByImages(string templatePath, string newFilePath, string imageFile, float absoluteX, float absoluteY)
        {
            PdfReader pdfReader = new PdfReader(templatePath);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFilePath, FileMode.Create));
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageFile);
            float percentage = 1;
            float resizedWidht = image.Width;
            float resizedHeight = image.Height;
            while (resizedHeight > 25)
            {
                percentage = percentage * 0.9f;
                resizedHeight = image.Height * percentage;
                resizedWidht = image.Width * percentage;
            }
            image.ScalePercent(percentage * 100);
            image.SetAbsolutePosition(absoluteX, absoluteY);
            PdfContentByte under = pdfStamper.GetUnderContent(1);
            under.AddImage(image);
            pdfStamper.Close();
            pdfReader.Close();
        }
    }
}
