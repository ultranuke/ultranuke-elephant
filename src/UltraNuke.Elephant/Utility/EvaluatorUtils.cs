﻿using System.Data;

namespace UltraNuke.Utility
{
    public class EvaluatorUtils
    {
        public static T Eval<T>(string statement) 
        {
            statement = statement.Replace("≥", ">=");
            statement = statement.Replace("＞", ">");
            statement = statement.Replace("≤", "<=");
            statement = statement.Replace("＜", "<");
            statement = statement.Replace("＝", "=");
            statement = statement.Replace("≠", "<>");

            DataTable dt = new DataTable();
            return (T)dt.Compute(statement, "false");
        }
    }
}
