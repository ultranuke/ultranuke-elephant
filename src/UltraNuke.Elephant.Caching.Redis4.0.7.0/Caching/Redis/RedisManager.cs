﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.Redis;

namespace UltraNuke.Caching.Redis
{
    public class RedisManager : ICache
    {
        #region Private Fields
        private RedisClient cache;
        #endregion

        #region Constructors
        public RedisManager()
        {
            cache = new RedisClient();
        }
        #endregion

        #region Public Members
        public bool Contains(string key)
        {
            return cache.Exists(key)>0;
        }

        public void Set(string key, object value)
        {
            cache.Set(key, value);
        }

        public void Set<T>(string key, T value)
        {
            cache.Set(key, value);
        }

        public object Get(string key)
        {
            return cache.Get(key);
        }

        public T Get<T>(string key)
        {
            return cache.Get<T>(key);
        }

        public void Remove(string key)
        {
            cache.Remove(key);
        }
        #endregion
    }
}
