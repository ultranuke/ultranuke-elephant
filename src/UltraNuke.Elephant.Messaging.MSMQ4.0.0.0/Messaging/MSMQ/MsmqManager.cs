﻿using System;
using MSMessaging = System.Messaging;

namespace UltraNuke.Messaging.MSMQ
{
    public class MsmqManager : DisposableObject, IMessageQueue
    {
        #region Private Fields
        private MSMessaging.MessageQueue msMessageQueue;
        #endregion

        #region Constructors
        public MsmqManager(string queue)
        {
            if (MSMessaging.MessageQueue.Exists(queue))
            {
                msMessageQueue = new MSMessaging.MessageQueue(queue);
            }
            else
            {
                msMessageQueue = MSMessaging.MessageQueue.Create(queue);
            }
        }
        #endregion

        #region Protected Methods
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (msMessageQueue != null)
                    msMessageQueue.Dispose();
            }
        }
        #endregion

        #region IMessageQueue Members
        public void Send<T>(string queue, T message) where T : class
        {
            try
            {
                MSMessaging.Message msMessage = new MSMessaging.Message();
                msMessage.Body = message;
                msMessage.Formatter = new MSMessaging.XmlMessageFormatter(new Type[] { typeof(T) });
                msMessageQueue.Send(msMessage);
            }
            catch (ArgumentException e)
            {
                throw new Exception(e.ToString());
            }
        }

        public T Receive<T>(string queue) where T : class
        {
            msMessageQueue.Formatter = new MSMessaging.XmlMessageFormatter(new Type[] { typeof(T) });
            try
            {
                MSMessaging.Message msMessage = msMessageQueue.Receive(new TimeSpan(0, 0, 6));
                T context = (T)msMessage.Body;
                return context;
            }
            catch (MSMessaging.MessageQueueException e)
            {
                throw new Exception(e.ToString());
            }
            catch (InvalidCastException e)
            {
                throw new Exception(e.ToString());
            }
        }
        #endregion
    }
}
