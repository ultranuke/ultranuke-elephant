﻿using System.Collections.Generic;
using System.Data;
using dap = Dapper;

namespace UltraNuke.Data.Dapper
{
    public class DapperManager : IData
    {
        #region Public Members
        public int Execute(IDbConnection conn, string sql, dynamic param = null, IDbTransaction transaction = null, CommandType? commandType = null)
        {
            return dap.SqlMapper.Execute(conn, sql, param, transaction, null, commandType);
        }

        public IEnumerable<T> Query<T>(IDbConnection conn, string sql, dynamic param = null, IDbTransaction transaction = null, CommandType? commandType = null)
        {
            return dap.SqlMapper.Query<T>(conn, sql, param, transaction, true, null, commandType);
        }
        #endregion
    }
}
