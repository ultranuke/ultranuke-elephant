﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UltraNuke.Messaging;
using UltraNuke.Messaging.MSMQ;

namespace UltraNuke.Elephant.Test.Messaging.MSMQ
{
    
    
    /// <summary>
    ///这是 IMessageQueueTest 的测试类，旨在
    ///包含所有 IMessageQueueTest 单元测试
    ///</summary>
    [TestClass()]
    public class IMessageQueueTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        // 
        //编写测试时，还可使用以下特性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        private string queuePath = @".\private$\DevXTestQueue";

        internal virtual IMessageQueue CreateIMessageQueue()
        {
            // TODO: 实例化相应的具体类。
            IMessageQueue target = new MsmqManager(queuePath);
            return target;
        }

        [TestMethod()]
        public void ReceiveTest()
        {
            IMessageQueue target = CreateIMessageQueue();
            string queue = queuePath;
            SampleMessage expected = new SampleMessage { Title = "ok" };
            SampleMessage actual;
            actual = target.Receive<SampleMessage>(queue);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SendTest()
        {
            IMessageQueue target = CreateIMessageQueue();
            string queue = queuePath;
            SampleMessage message = new SampleMessage { Title = "ok" };
            target.Send<SampleMessage>(queue, message);
        }
    }
}
