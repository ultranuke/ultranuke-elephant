﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UltraNuke.Caching;
using UltraNuke.Caching.Redis;

namespace UltraNuke.Elephant.Test.Caching.Redis
{
    
    
    /// <summary>
    ///这是 ICacheTest 的测试类，旨在
    ///包含所有 ICacheTest 单元测试
    ///</summary>
    [TestClass()]
    public class ICacheTestForRedis
    {
        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        // 
        //编写测试时，还可使用以下特性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        internal virtual ICache CreateICache()
        {
            // TODO: 实例化相应的具体类。
            ICache target = new RedisManager();
            return target;
        }

        /// <summary>
        ///Contains 的测试
        ///</summary>
        [TestMethod()]
        public void ContainsTest()
        {
            ICache target = CreateICache();
            string key = "name1";
            bool expected = false;
            bool actual;
            actual = target.Contains(key);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Get<T> 的测试
        ///</summary>
        [TestMethod()]
        public void GetTestForT()
        {
            SetExample();
            ICache target = CreateICache();
            string key = "name";
            string expected = "li xiao fang";
            string actual;
            actual = target.Get<string>(key);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Get 的测试
        ///</summary>
        [TestMethod()]
        public void GetTest()
        {
            SetExample();
            ICache target = CreateICache();
            string key = "name";
            string expected = "li xiao fang";
            string actual;
            actual = (string)target.Get(key);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///Remove 的测试
        ///</summary>
        [TestMethod()]
        public void RemoveTest()
        {
            SetExample();
            ICache target = CreateICache();
            string key = "name";
            target.Remove(key);
            Assert.Inconclusive("无法验证不返回值的方法。");
        }

        /// <summary>
        ///Set 的测试
        ///</summary>
        [TestMethod()]
        public void SetTest()
        {
            SetExample();
            Assert.Inconclusive("验证此测试方法的正确性。");
        }

        private void SetExample()
        {
            ICache target = CreateICache();
            string key = "name";
            string value = "li xiao fang";
            target.Set(key, value);
        }
    }
}
