﻿using System;
using UltraNuke.Delphinidae;

namespace UltraNuke.Elephant.Test.Repositories.Aggregates
{
    public class BaseTestDomain : AggregateRoot
    {
        #region Constructors
        public BaseTestDomain()
            : base()
        {
        }

        public BaseTestDomain(Guid id)
            : base(id)
        {
        }
        #endregion

        #region Public Properties
        public virtual string Code { get; protected set; }
        public virtual string Domain { get; protected set; }
        #endregion        

        #region Public Methods
        public static BaseTestDomain Create(Guid? id,
            string code,
            string domain)
        {
            BaseTestDomain baseTestDomain = null;
            if (id != null)
                baseTestDomain = new BaseTestDomain(id.Value);
            else
                baseTestDomain = new BaseTestDomain();

            baseTestDomain.Code = code;
            baseTestDomain.Domain = domain;

            baseTestDomain.AggregateState = AggregateState.Added;
            return baseTestDomain;
        }

        public static BaseTestDomain Create(
            string code,
            string domain)
        {
            return Create(null, code, domain);
        }

        public virtual void UpdateInformation(string code,
            string domain)
        {
            this.Code = code;
            this.Domain = domain;
        }

        public virtual void Delete()
        {
            this.AggregateState = AggregateState.Deleted;
        }
        #endregion
    }
}
