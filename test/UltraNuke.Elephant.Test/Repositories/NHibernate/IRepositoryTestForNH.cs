﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernateCfg = NHibernate.Cfg;
using System.Linq;
using UltraNuke.Elephant.Test.Repositories.Aggregates;
using Spring.Objects.Factory.Support;
using CommonServiceLocator.SpringAdapter;
using UltraNuke.Repositories.NHibernate;
using Microsoft.Practices.ServiceLocation;
using UltraNuke.Repositories;

namespace UltraNuke.Elephant.Test.Repositories.EntityFramework
{
    [TestClass]
    public class IRepositoryTestForNH
    {
        #region 附加测试特性
        // 
        //编写测试时，还可使用以下特性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}

        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        protected IRepository GetRepository()
        {
            DefaultListableObjectFactory objectFactory = new DefaultListableObjectFactory(false);

            NHibernateContext ctx = new NHibernateContext(new NHibernateCfg.Configuration().Configure(AppDomain.CurrentDomain.BaseDirectory + @"\Repositories\NHibernate\NHibernate.cfg.xml"));
            objectFactory.RegisterSingleton(typeof(NHibernateContext).FullName, ctx);
            objectFactory.RegisterSingleton(typeof(NHibernateRepository).FullName, new NHibernateRepository(ctx));
            ServiceLocator.SetLocatorProvider(() => new SpringServiceLocatorAdapter(objectFactory));

            IRepository repository = ServiceLocator.Current.GetInstance<IRepository>();
            return repository;
        }

        [TestMethod]
        public void ExecuteCRUD()
        {
            Guid id;
            IRepository repository = GetRepository();
            BaseTestDomain createDomain = BaseTestDomain.Create("T0001", "测试项目001");
            repository.Save(createDomain);
            id = createDomain.Id;
            repository = GetRepository();
            BaseTestDomain expectedDomain = repository.Get<BaseTestDomain>(id);
            Assert.AreEqual("测试项目001", expectedDomain.Domain);

            repository = GetRepository();
            BaseTestDomain updateDomain = repository.Get<BaseTestDomain>(id);
            updateDomain.UpdateInformation("T0001", "测试项目002");
            repository.Save(updateDomain);
            repository = GetRepository();
            expectedDomain = repository.Get<BaseTestDomain>(id);
            Assert.AreEqual("测试项目002", expectedDomain.Domain);

            repository = GetRepository();
            expectedDomain = repository.Query<BaseTestDomain>().Where(w => w.Code.Equals("T0001")).FirstOrDefault();
            Assert.AreEqual("测试项目002", expectedDomain.Domain);

            repository = GetRepository();
            BaseTestDomain deleteDomain = repository.Get<BaseTestDomain>(id);
            deleteDomain.Delete();
            repository.Save(deleteDomain);
            repository = GetRepository();
            expectedDomain = repository.Get<BaseTestDomain>(id);
            Assert.IsNull(expectedDomain);
        }
    }
}
