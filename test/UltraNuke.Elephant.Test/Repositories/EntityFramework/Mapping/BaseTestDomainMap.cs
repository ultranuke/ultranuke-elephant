﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using UltraNuke.Elephant.Test.Repositories.Aggregates;

namespace UltraNuke.Elephant.Test.Repositories.EntityFramework
{
    public class BaseTestDomainMap : EntityTypeConfiguration<BaseTestDomain>
    {
        public BaseTestDomainMap()
        {
            this.ToTable("ET_BaseTestDomain");

            this.Ignore(t => t.AggregateState);
        }
    }
}
