﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace UltraNuke.Elephant.Test.Repositories.EntityFramework
{
    public class EFTestContext : DbContext
    {
        static EFTestContext()
        {
            Database.SetInitializer<EFTestContext>(null);
        }

        public EFTestContext() : base("name=DBConnectionString") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BaseTestDomainMap());
        }
    }
}
