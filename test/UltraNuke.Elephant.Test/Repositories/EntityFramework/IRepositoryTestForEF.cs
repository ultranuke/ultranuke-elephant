﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using UltraNuke.Elephant.Test.Repositories.Aggregates;
using Microsoft.Practices.ServiceLocation;
using UltraNuke.Repositories;
using Spring.Objects.Factory.Support;
using UltraNuke.Repositories.EntityFramework;
using CommonServiceLocator.SpringAdapter;

namespace UltraNuke.Elephant.Test.Repositories.EntityFramework
{
    [TestClass]
    public class IRepositoryTestForEF
    {
        #region 附加测试特性
        // 
        //编写测试时，还可使用以下特性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        protected IRepository GetRepository()
        {

            DefaultListableObjectFactory objectFactory = new DefaultListableObjectFactory(false);
            EntityFrameworkContext ctx = new EntityFrameworkContext(new EFTestContext());
            objectFactory.RegisterSingleton(typeof(EntityFrameworkContext).FullName, ctx);
            objectFactory.RegisterSingleton(typeof(EntityFrameworkRepository).FullName, new EntityFrameworkRepository(ctx));
            ServiceLocator.SetLocatorProvider(() => new SpringServiceLocatorAdapter(objectFactory));

            IRepository repository = ServiceLocator.Current.GetInstance<IRepository>();
            return repository;
        }

        [TestMethod]
        public void ExecuteCRUD()
        {
            Guid id;
            IRepository repository = GetRepository();
            BaseTestDomain createDomain = BaseTestDomain.Create("T0001", "测试项目001");
            repository.Save(createDomain);
            id = createDomain.Id;
            repository = GetRepository();
            BaseTestDomain expectedDomain = repository.Get<BaseTestDomain>(id);
            Assert.AreEqual("测试项目001", expectedDomain.Domain);

            repository = GetRepository();
            BaseTestDomain updateDomain = repository.Get<BaseTestDomain>(id);
            updateDomain.UpdateInformation("T0001", "测试项目002");
            repository.Save(updateDomain);
            repository = GetRepository();
            expectedDomain = repository.Get<BaseTestDomain>(id);
            Assert.AreEqual("测试项目002", expectedDomain.Domain);

            repository = GetRepository();
            expectedDomain = repository.Query<BaseTestDomain>().Where(w => w.Code.Equals("T0001")).FirstOrDefault();
            Assert.AreEqual("测试项目002", expectedDomain.Domain);

            repository = GetRepository();
            BaseTestDomain deleteDomain = repository.Get<BaseTestDomain>(id);
            deleteDomain.Delete();
            repository.Save(deleteDomain);
            repository = GetRepository();
            expectedDomain = repository.Get<BaseTestDomain>(id);
            Assert.IsNull(expectedDomain);
        }
    }
}
